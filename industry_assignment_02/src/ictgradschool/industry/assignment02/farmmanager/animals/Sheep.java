package ictgradschool.industry.assignment02.farmmanager.animals;

/**
 * Created by mshe666 on 1/12/2017.
 */
public class Sheep extends Animal implements IProductionAnimal{
    private final int MAX_VALUE = 1200;

    public Sheep() {
        value = 800;
    }

    public void feed() {
        if (value < MAX_VALUE) {
            value += 100;
        }
    }

    public int costToFeed() {
        return 10;
    }

    public String getType() {
        return "Sheep";
    }

    public String toString() {
        return getType() + " - $" + value;
    }

    @Override
    public boolean harvestable() {
        boolean canHarvet = false;
        if (this.value >= MAX_VALUE) {
            canHarvet = true;
        }
        return canHarvet;
    }

    @Override
    public int harvest() {
        if (this.harvestable()) {
            return 60;
        }
        return 0;
    }
}
